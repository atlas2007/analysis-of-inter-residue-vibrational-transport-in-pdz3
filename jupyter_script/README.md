# Purpose
  - Contains scripts used to analyze scaling relations in the pdz3 protein. They
    are stored in the form of jupyter notebooks and associated data. 

# Usage
  - type jupyter notebook in this directory, open any of the notebooks, and
    follow instructions to make figures, or look at the code used. 

## jupyter notebook files
  - compute\_all.ipynb: 
  - hydrogen\_bond.ipynb:
  - histogram.ipynb: 
  - charged\_contact.ipynb: 
  - irEC\_vs\_irHC.ipynb: 

## directories
  - crd\_analysis: residue pair related data, atomic pair related data,
    hydrogen bond dynamics related data like fluctuation/variance, and charged
    contact dynamics related data like mean square distance

  - out: collection of useful data made using data from crd\_analysis, often
    regarding collections of residue pair data, or collection of conductivity
    data.

  - fig: all figures made using jupyter notebook are stored here
