# Purpose
    I clarify some short hands that I use to name files, so that the contents
    may be understood easier.

# Short hands
    - h: usually h stands for hydrogen bonds
    - ch: charged contacts
    - unchar: not charged
    - ct: contact or tertiary contact residue pairs
    - bb: backbone residue pairs
    - pairs: residue pair data
    - gpair: group pairs
    - top: referes to residue pairs with relatively large conductivities
    - bottom: residue pairs with relatively low conductivities
    - com: center of mass
    - ec: energy conductivity
    - hc: heat conductivity
    - tc: transport coefficient
    - lambda: thermal conductivity
    - block: matrix of data
    - conduc: conductivities like ec, hc, or lambda
    - dynamic: time averaged distance variables like variance, avg distance and
      mean square displacement
