# Analysis of vibrational transport in pdz3
  Analysis scripts and data used to make figures that show scaling between 
  transport coefficients and dynamics of non-bonded contacts. Scripts are 
  stored in the form of jupyter notebook.

## Usage
  1. git clone this repository
  2. move to jupyter_script directory and use the jupyter notebook command
  3. As an outlier, the script used in finding hydrogen bonds is in bash
       script form, and is stored in store-data/traj/hbond.sh. 

## Rough Content
  - calculate hydrogen bond distances, and variances
  - calculate charged contact distances, variances, and mean square
    displacements
  - make figures that show scaling between transport coeffients and non-bonded
    contact dynamics
  - finding hydrogen bonds using the cpptraj hbond command

## Directories
  - jupyter_scripts: contains the jupyter notebook scripts, analysis data, and
    figures showing scaling.

  - store-data: stores transport coefficient data, trajectory data and the
    bash script used to compute hydrogen bonds.
