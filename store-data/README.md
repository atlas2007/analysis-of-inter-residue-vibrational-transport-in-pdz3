# store-data

# Purpose
  Store data computed using yamato group cluster. They include irec and irhc
  results for both 150ps0.5fs trajectories, and 1ns2fs trajectories. This
  direcory also stores found hydrogen bond data calculated using cpptraj
  hbond.

# Note
  - lambda data is stored in the same directory as those for irhc, ie /hflux/.

# Organization
  - eflux: irec data computed from 150ps0.5fs trajectories
  - eflux_2fs1ns: irec data computed from 1ns2fs trajectories
  - eflux_oliv: irec data computed by olivier on 1ns2fs trajectories created
    by him
  - hflux: irhc data and lambda computed from 150ps0.5fs trajectories
  - hflux_2fs1ns: irhc data computed from 1ns2fs trajectories
  - hflux_oliv: irhc data computed by olivier on 1ns2fs trajectories created
    by him
  - traj: stores one of the 150ps0.5fs trajectories, and a bash script used to
    find hydrogen bond contacts for a given trajectory.

