# traj

# Purpose
    Store one example of trajectory, and a method to calculate hydrogen bonds.

# Organization
    - 000: the first trajectory from 100 trajectories of 150ps0.5fs
    - hbond.sh: bash script used to compute hydrogen bonds.
        usage:
            1. edit the hbond.sh file and specify, of which file you want to
               compute the hydrogen bond for.
            2. run 'bash hbond.sh'
    - stripped.prmtop: topology file necessary in using hbond.sh
