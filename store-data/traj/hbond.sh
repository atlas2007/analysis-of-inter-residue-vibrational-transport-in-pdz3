#! /bin/bash -e
# identify hbond for each trajectory

for i in 0{00..99}
do
cd $i

cpptraj << EOF
parm ../stripped.prmtop
trajin strip-00.nccrd
trajin strip-01.nccrd
trajin strip-02.nccrd
trajin strip-03.nccrd
trajin strip-04.nccrd
trajin strip-05.nccrd
trajin strip-06.nccrd
trajin strip-07.nccrd
trajin strip-08.nccrd
trajin strip-09.nccrd
hbond All out hbvtime_all.dat avgout avghb_all.dat
run
EOF

cd ..
done
